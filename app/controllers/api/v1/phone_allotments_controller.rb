class Api::V1::PhoneAllotmentsController < ApplicationController

  def allott_phone_numbers
    ph_no_record = Phone.where(allotted: false)
    if ph_no_record.present?
      @allotted_ph = ph_no_record.first
      save_phone_allottment
    else
      render json: { status: 'FAILURE', message: 'No free Phone Numbers' }, status: :ok
    end
  end

  def save_client_specific_allottment
    if invalid_ph_no(params[:phone_number])
      render json: { status: 'FAILURE', message: 'Invalid Format'}, status: :ok
    else
      ph_record = Phone.where(phone_number: params[:phone_number])
      if ph_record.present? && !ph_record.first.allotted
        @allotted_ph = ph_record.first
        save_phone_allottment
      elsif ph_record.blank?
        @allotted_ph = Phone.where(allotted: false).first
        save_phone_allottment
      else
        render json: { status: 'FAILURE', message: 'Phone Number Already Allotted' }, status: :ok
      end
    end
  end

  def save_phone_allottment
    phone_allottment = PhoneAllotment.create(phone_number: @allotted_ph.phone_number, 
                                               client_name: params[:client_name])
    if phone_allottment.save
      @allotted_ph.update!(allotted: true)
      render json: { status: 'SUCCESS', message: 'Phone number allotted successfully',
                     data: ActiveModelSerializers::SerializableResource
                            .new(phone_allottment, 
                             each_serializer: Api::V1::PhoneAllottmentSerializer).as_json }, status: :ok
    else
      render json: { status: 'FAILURE', message: 'Allottment Failed' }, status: :unprocessable_entity
    end
  end

  private

    def invalid_ph_no(phone_number)
      invalid = true
      if /\(?[0-9]{3}\)?-[0-9]{3}-[0-9]{4}/.match?(phone_number)
        invalid = false
      end
      return invalid
    end
end
