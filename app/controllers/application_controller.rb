class ApplicationController < ActionController::API
  before_action :authorize

  def authorize
    if authorize_headers == 'application/json' || authorize_headers == 'multipart/form-data'
      true
    else
      render json: { message: I18n.t('authorization.unauthorized'),
                     status: 401 }, status: :unauthorized
    end
  end

  private

  def authorize_headers
    request.content_type
  end
end
