class Api::V1::PhoneAllottmentSerializer < ActiveModel::Serializer
  attributes :id, :phone_number, :client_name
end
