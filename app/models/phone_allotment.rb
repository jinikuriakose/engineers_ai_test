class PhoneAllotment < ApplicationRecord
  validates_format_of :phone_number, :with => /\(?[0-9]{3}\)?-[0-9]{3}-[0-9]{4}/, :message => "- Phone numbers must be in xxx-xxx-xxxx format."
  validates_presence_of :client_name
end
