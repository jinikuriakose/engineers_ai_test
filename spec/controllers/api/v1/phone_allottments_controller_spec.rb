# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::PhoneAllotmentsController, type: :controller do
  describe 'GET phone_allotments#allott_phone_numbers' do
    before do
      get :allott_phone_numbers
    end

    it 'should return unauthorized if content type is not application/json' do
      expect(response).to have_http_status(:unauthorized)
    end
  end
  describe 'GET phone_allotments#allott_phone_numbers' do
    context 'it should create phone allottments' do
      before do
        request.content_type = 'application/json'
      end

      it 'should create new phone allottments' do
        get :allott_phone_numbers, params: {client_name: 'Sample'}
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe 'GET phone_allotments#save_client_specific_allottment' do
    context 'it should create client specific phone allottments' do
      before do
        request.content_type = 'application/json'
      end

      it 'should create new phone allottments' do
        get :save_client_specific_allottment, params: {phone_number: '111-111-1119', client_name: 'Sample'}
        expect(response).to have_http_status(:success)
      end

      it 'should not create new phone allottments' do
        get :save_client_specific_allottment, params: {phone_number: '111-1111119', client_name: 'Sample'}
        json_response = JSON.parse(response.body)
        expect(json_response['status']).to eq('FAILURE')
      end
    end
  end
end