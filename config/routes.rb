Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace 'api' do
    namespace 'v1' do
      resources :phone_allotments do
        collection do
          post :allott_phone_numbers
          post :save_client_specific_allottment
        end
      end
    end
  end
end
