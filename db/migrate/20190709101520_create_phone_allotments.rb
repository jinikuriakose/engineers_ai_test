class CreatePhoneAllotments < ActiveRecord::Migration[5.2]
  def change
    create_table :phone_allotments do |t|
      t.string :phone_number
      t.string :client_name
      t.timestamps
    end
  end
end
